﻿Feature: Walking Skeleton
    As an eXtreme Programming Developer
    I want testing infrastructure setup and intial architecture
     In order to deliver at a sustainable pace
@mytag
Scenario: Listing the items stored in a database
 Given the database contains the following items
 | ItemText |
 | Item1    |
 | Item2    |
 | Item3    |
 | Item4    |
 When I visit "~/"
 Then I should see the following items on the page
 | ItemText |
 | Item1    |
 | Item2    |
 | Item3    |
 | Item4    |