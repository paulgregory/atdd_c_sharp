﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using ATDD_Demo_MVC.Models;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Firefox;
using OpenQA.Selenium.IE;
using TechTalk.SpecFlow;
using TechTalk.SpecFlow.Assist;

namespace ATTD_Demo_MVC.AcceptanceTests
{
 /*   
    public class ItemDbContext : DbContext
    {
        public DbSet<Item> Items { get; set; }
    }
    public class Item
    {
        public int Id { get; set; }
        public string ItemText { get; set; }
    }
   */
 



    [Binding]
    public class WalkingSkeletonSteps
    {
        private static ItemDbContext _db = new ItemDbContext();
        private IWebDriver _driver = new ChromeDriver("C:/Selenium");
        //private IWebDriver _driver = new FirefoxDriver();

        [BeforeScenario]
        public static void BeforeScenario()
        {
            _db.Database.ExecuteSqlCommand("TRUNCATE TABLE Items;");
            _db.SaveChanges();
        }

        [Given(@"the database contains the following items")]
        public void GivenTheDatabaseContainsTheFollowingItems(Table table)
        {
            IEnumerable<Item> givenItems = table.CreateSet<Item>();
            foreach (var givenItem in givenItems)
            {
                _db.Items.Add(givenItem);
            }
            _db.SaveChanges();
        }

        [When(@"I visit ""(.*)""")]
        public void WhenIVisit(string p0)
        {

            _driver.Navigate().GoToUrl("http://localhost:4000/WalkingSkeleton/Items");
            
        }

        [Then(@"I should see the following items on the page")]
        public void ThenIShouldSeeTheFollowingItemsOnThePage(Table table)
        {
            IEnumerable<Item> expectedItems = table.CreateSet<Item>();
            IEnumerable<String> itemsText = expectedItems.Select(i => i.ItemText);

            IEnumerable<IWebElement> itemWebElements = _driver.FindElements(By.ClassName("item"));

            Assert.AreEqual(itemsText.Count(), itemWebElements.Count(), String.Format("Expected {0} items on website", itemsText.Count()));

            for (var i = 0; i < itemWebElements.Count(); i++)
            {
                Assert.AreEqual(itemWebElements.ElementAt(i).Text, itemsText.ElementAt(i));
            }
            _driver.Quit();
        }
    }
}
