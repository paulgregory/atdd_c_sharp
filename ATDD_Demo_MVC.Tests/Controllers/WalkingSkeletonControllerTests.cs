﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using ATDD_Demo_MVC.Controllers;
using ATDD_Demo_MVC.Controllers.Interfaces;
using ATDD_Demo_MVC.Models;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;

namespace ATDD_Demo_MVC.Tests.Controllers
{
    [TestClass]
    public class WalkingSkeletonControllerTests
    {
        [TestMethod]
        public void WalkingSkeletonControllerTest()
        {
            Mock<ItemsRepository> itemsRepository = new Mock<ItemsRepository>();

            WalkingSkeletonController walkingSkeletonController = new WalkingSkeletonController(itemsRepository.Object);
            ViewResult result = walkingSkeletonController.Items() as ViewResult;
            Assert.IsNotNull(result);
        }

        [TestMethod]
        public void TestThatItemsActionRendersTheItemsView()
        {
            Mock<ItemsRepository> itemsRepository = new Mock<ItemsRepository>();

            WalkingSkeletonController walkingSkeletonController = new WalkingSkeletonController(itemsRepository.Object);
            ViewResult result = walkingSkeletonController.Items() as ViewResult;
            Assert.AreEqual("Items", result.ViewName);
        }

        [TestMethod]
        public void TestThatTheControllerProvidesTheViewWithCollectionOfItems()
        {

            IEnumerable<Item> expectedItems = new List<Item>
            {
                new Item {ItemText = "Item1"},
                new Item {ItemText = "Item2"}
            };
            Mock<ItemsRepository> itemsRepository = new Mock<ItemsRepository>();

            itemsRepository.Setup(repository => repository.GetItems()).Returns(expectedItems);
            WalkingSkeletonController walkingSkeletonController = new WalkingSkeletonController(itemsRepository.Object);
            ViewResult result = walkingSkeletonController.Items() as ViewResult;
            IEnumerable<Item> actual = (IEnumerable<Item>) result.ViewData.Model;
            Assert.IsNotNull(actual);
            itemsRepository.Verify(rcv => rcv.GetItems(), Times.Exactly(1));
            CollectionAssert.AreEqual(expectedItems.ToList(), actual.ToList());
        }
    }
}
