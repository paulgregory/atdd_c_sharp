﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ATDD_Demo_MVC.Models;

namespace ATDD_Demo_MVC.Controllers.Interfaces
{
    public interface ItemsRepository
    {
        IEnumerable<Item> GetItems();
    }
}
