﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ATDD_Demo_MVC.Controllers.Interfaces;
using ATDD_Demo_MVC.Models.Repositories;

namespace ATDD_Demo_MVC.Controllers
{
    public class WalkingSkeletonController : Controller
    {
        
        private ItemsRepository _itemsRepository;
        
        public WalkingSkeletonController()
            : this(new ItemsRespositoryIpml())
        {

        }

        public WalkingSkeletonController(ItemsRepository repository)
        {
            _itemsRepository = repository;
        }
        
        //
        // GET: /WalkingSkeleton/
        public ActionResult Items()
        {
            return View("Items", _itemsRepository.GetItems());
            //return View("Items");

        }
    }
}