﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ATDD_Demo_MVC.Controllers.Interfaces;

namespace ATDD_Demo_MVC.Models.Repositories
{
    public class ItemsRespositoryIpml : ItemsRepository
    {
        ItemDbContext _db = new ItemDbContext();

        public IEnumerable<Item> GetItems()
        {
            return _db.Items.ToList();
        }
    }
}