﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace ATDD_Demo_MVC.Models
{
    public class ItemDbContext : DbContext
    {
        public DbSet<Item> Items { get; set; }
    }
    public class Item
    {
        public int Id { get; set; }
        public string ItemText { get; set; }
    }
}